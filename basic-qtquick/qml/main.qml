import QtQuick 2.6
import QtQuick.Window 2.0
import QtQuick.Controls 2.15
// Import our Rust classes
import Greeter 1.0

Window {
    visible: true
    // Instantiate the rust struct
    Greeter {
        id: greeter
        // Set a property
        name: "World"
    }
    Button {
        anchors.centerIn: parent
        // Call a method
        text: "Click Me"
        onClicked: {
            console.log("Logging from QML: " + greeter.compute_greetings(
                            "hello"))
        }
    }
}
