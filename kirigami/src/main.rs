use cstr::cstr;
use ki18n::klocalizedcontext::KLocalizedContext;
use qmetaobject::{prelude::*, QUrl};

// The `QObject` custom derive macro allows to expose a class to Qt and QML
#[derive(QObject, Default)]
struct Greeter {
    // Specify the base class with the qt_base_class macro
    base: qt_base_class!(trait QObject),
    // Declare `name` as a property usable from Qt
    name: qt_property!(QString; NOTIFY name_changed),
    // Declare a signal
    name_changed: qt_signal!(),
    // And even a slot
    compute_greetings: qt_method!(
        fn compute_greetings(&self, verb: String) -> QString {
            format!("{} {}", verb, self.name.to_string()).into()
        }
    ),
}

// Setup QRC
qrc!(root_qml,
    "" {
        "qml/main.qml" as "main.qml",
    }
);

fn main() {
    // Setup Logging
    qmetaobject::log::init_qt_to_rust();
    env_logger::init();
    log::info!("Starting Application");

    // Register the `Greeter` struct to QML
    qml_register_type::<Greeter>(cstr!("Greeter"), 1, 0, cstr!("Greeter"));

    // For QRC
    root_qml();

    // Create a QML engine from rust
    let mut engine = QmlEngine::new();

    // Initialize KLocalizedContext
    KLocalizedContext::init_from_engine(&engine);

    // Loading QML from file.
    engine.load_url(QUrl::from(QString::from("qrc:///main.qml")));
    engine.exec();
}
