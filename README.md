# Introduction
This repository contains templates for gerneating RUST + QT projects using [cargo-generate](https://github.com/cargo-generate/cargo-generate).

# Basic QtQuick Application
## Dependencies
Some Basic dependencies need to be installed in the system.
### Manual
QT can be installed by using the QT installer for Linux. Remember to install qmake since it is used to detect the QT install instructions.
### Ubuntu
```
sudo apt install build-essential qtbase5-dev qtdeclarative5-dev libqt5svg5-dev qtquickcontrols2-5-dev qml-module-qtquick-layouts
```
## Generate Project
```
cargo generate --git https://invent.kde.org/oreki/rust-qt-template/ basic-qtquick --name myproject
```

# Kirigami Application
## Dependencies
Some Basic dependencies need to be installed in the system.
### Manual
QT can be installed by using the QT installer for Linux. Remember to install qmake since it is used to detect the QT install instructions.
### Ubuntu
```
sudo apt install build-essential qtbase5-dev qtdeclarative5-dev libqt5svg5-dev qtquickcontrols2-5-dev qml-module-qtquick-layouts qml-module-org-kde-kirigami2 kirigami2-dev libkf5i18n-dev gettext libkf5coreaddons-dev libkf5kdelibs4support5-bin
```
## Generate Project
```
cargo generate --git https://invent.kde.org/oreki/rust-qt-template/ kirigami --name myproject
```

# Helpful Crates for Rust 
- [qmetaobject](https://crates.io/crates/qmetaobject): Allows to create QT/QML applications with Rust.
- [ki18n](https://crates.io/crates/ki18n/): A wrapper for KI18n Framework by KDE.
- [kconfig](https://invent.kde.org/oreki/kconfig-rs): A wrapper for KConfig Framework by KDE.
- [cpp](https://crates.io/crates/cpp): A crate that allows embedding C++ code directly in Rust.
- [cstr](https://crates.io/crates/cstr/0.2.9): Provides macro to create `&'static CStr` from literal or identifier.
